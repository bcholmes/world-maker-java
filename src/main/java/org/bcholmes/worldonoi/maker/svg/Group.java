package org.bcholmes.worldonoi.maker.svg;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "g")
@Namespace(reference = "http://www.w3.org/2000/svg")
public class Group {

	@ElementListUnion({
		@ElementList(entry="path",type=Path.class, inline=true)
	})
	List<Object> objects = new ArrayList<>();
	
	@Attribute
	String id;
	
	@Attribute(required = false)
	@Namespace(reference="http://www.inkscape.org/namespaces/inkscape", prefix="inkscape")
	String label;
	
	@Attribute(required = false)
	@Namespace(reference="http://www.inkscape.org/namespaces/inkscape", prefix="inkscape")
	String groupmode;
	

	public Group(String id) {
		this.id = id;
	}
	public Group(String id, String layerId) {
		this.id = id;
		this.label = layerId;
		this.groupmode = "layer";
	}
	public Group() {
	}

	public List<Object> getObjects() {
		return objects;
	}

	public void setObjects(List<Object> objects) {
		this.objects = objects;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getGroupmode() {
		return groupmode;
	}
	public void setGroupmode(String groupmode) {
		this.groupmode = groupmode;
	}
}
