package org.bcholmes.worldonoi.maker.svg;

import de.alsclo.voronoi.graph.Point;

public interface Segment {
	public Point getStart();
	public Point getEnd();
	public Point getNeighbour();
	public Point getCenter();
}
