package org.bcholmes.worldonoi.maker.svg;

import java.util.List;

import org.bcholmes.worldonoi.maker.Cell.SegmentImpl;

public interface PathProvider {
	List<SegmentImpl> path();
}
