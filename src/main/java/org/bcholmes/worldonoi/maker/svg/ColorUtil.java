package org.bcholmes.worldonoi.maker.svg;

import java.awt.Color;

public class ColorUtil {

	public static String toHex(Color color) {
		return String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
	}
	
}
