package org.bcholmes.worldonoi.maker.svg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bcholmes.worldonoi.maker.Cell;
import org.bcholmes.worldonoi.maker.Cell.SegmentImpl;
import org.bcholmes.worldonoi.maker.ColorSymbol;
import org.bcholmes.worldonoi.maker.FeatureType;
import org.bcholmes.worldonoi.maker.WorldMap;

import de.alsclo.voronoi.graph.Point;

public class SvgWriter {
	
	static class SimplePathProvider implements PathProvider {

		private List<SegmentImpl> path;

		SimplePathProvider(List<SegmentImpl> path) {
			this.path = path;
		}
		
		@Override
		public List<SegmentImpl> path() {
			return this.path;
		}
		
	}

	private static Svg createSvgTemplate(WorldMap worldMap) {
		Svg result = new Svg();
		result.setWidth(worldMap.getSize().getWidth());
		result.setHeight(worldMap.getSize().getHeight());
		result.setViewBox("0 0 " + worldMap.getSize().getWidth() + " " + worldMap.getSize().getHeight());
		return result;
	}

	public static Svg toLayeredSvg(WorldMap worldMap) {
		Svg result = createSvgTemplate(worldMap);
		Map<FeatureType,Group> groups = new HashMap<>();

		Group waterLayer = new Group("water-layer", "water-layer");
		result.getObjects().add(waterLayer);
		groups.put(FeatureType.WATER, waterLayer);
		Group landLayer = new Group("land-layer", "land-layer");
		result.getObjects().add(landLayer);
		groups.put(FeatureType.LAND, landLayer);
		Group iceLayer = new Group("ice-layer", "ice-layer");
		result.getObjects().add(iceLayer);
		groups.put(FeatureType.ICE, iceLayer);
		
		
		for (ColorSymbol symbol : ColorSymbol.orderedValues()) {
			List<Cell> cells = getCellsInLayer(worldMap, symbol.getFeatureType(), symbol.elevation());
			List<SegmentImpl> segments = findEdges(cells);
			groups.get(symbol.getFeatureType()).getObjects().add(new Path(segmentsToPath(new SimplePathProvider(Cell.stitchTogetherPath(segments))), toStyle(symbol)));
		}
		return result;
	}
	
	private static List<SegmentImpl> findEdges(List<Cell> cells) {
		Set<String> centers = cells.stream().map(c -> renderCoordinate(c.getCenter())).collect(Collectors.toSet());
		Set<SegmentImpl> result = new HashSet<SegmentImpl>();
		cells.forEach(c -> {
			c.getSegments().forEach(s -> {
				if (s.getNeighbour() == null || !centers.contains(renderCoordinate(s.getNeighbour()))) {
					result.add(s);
				}
			});
		});
		return new ArrayList<>(result);
	}

	private static List<Cell> getCellsInLayer(WorldMap worldMap, FeatureType featureType, int elevation) {
		return worldMap.getCells()
				.stream()
				.filter(getFilter(featureType, elevation))
				.collect(Collectors.toList());
	}

	private static Predicate<? super Cell> getFilter(FeatureType featureType, int elevation) {
		if (featureType == FeatureType.LAND) {
			return c -> (c.getFeatureType() == featureType && c.getElevation() >= elevation) 
					|| (c.getFeatureType() == FeatureType.ICE && (c.getElevation() -1)  >= elevation);
		} else if (featureType == FeatureType.ICE) {
			return c -> c.getFeatureType() == featureType && c.getElevation() >= elevation;
		} else {
			return c -> c.getElevation() >= elevation;
		}
	}

	private static String toStyle(ColorSymbol colorSymbol) {
		return "fill:" + colorSymbol.getColor() 
				+ ";stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-rule:evenodd";
	}

	private static String segmentsToPath(PathProvider pathProvider) {
		StringBuilder builder = new StringBuilder();
		SegmentImpl first = null;
		SegmentImpl previous = null;
		for (SegmentImpl segment : pathProvider.path()) {
			if (first != null && segment.getEnd().equals(first.getStart())) {
				builder.append(" Z");
				first = null;
			} else {
				if (first == null || previous == null || !previous.getEnd().equals(segment.getStart())) {
					builder.append(" M").append(renderCoordinate(segment.getStart()));
					first = segment;
				}
				builder.append(" L").append(renderCoordinate(segment.getEnd()));
			}
			previous = segment;
		}
		return builder.toString().trim();
	}

	private static String renderCoordinate(Point point) {
		return String.format("%.6f,%.6f", point.x, point.y);
	}
}
