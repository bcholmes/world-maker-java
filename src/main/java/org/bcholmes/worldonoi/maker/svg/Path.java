package org.bcholmes.worldonoi.maker.svg;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "path")
@Namespace(reference = "http://www.w3.org/2000/svg")
public class Path {

	@Attribute(name = "d")
	String description;
	
	@Attribute
	String style = "fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1";
	
	public Path() {
	}

	public Path(String description, String style) {
		this.description = description;
		this.style = style;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
}
