package org.bcholmes.worldonoi.maker.svg;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name = "svg")
@Namespace(reference = "http://www.w3.org/2000/svg")
public class Svg {

	@Attribute
	int width;
	@Attribute
	int height;
	@Attribute
	String viewBox;
	@Attribute
	String version = "1.1";
	
	@ElementListUnion({
		@ElementList(entry="path",type=Path.class, inline=true),
		@ElementList(entry="g",type=Group.class, inline=true)
	})
	List<Object> objects = new ArrayList<>();
	
	public Svg() {
	}
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getViewBox() {
		return viewBox;
	}
	public void setViewBox(String viewBox) {
		this.viewBox = viewBox;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	public List<Object> getObjects() {
		return objects;
	}
}
