package org.bcholmes.worldonoi.maker;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public enum ColorSymbol {

	LAND_1("#84b677"),
	LAND_2("#96be82"),
	LAND_3("#a8c78d"),
	LAND_4("#bcd29a"),
	LAND_5("#cedaa5"),
	LAND_6("#e0e4b1"),
	LAND_7("#e6e2b4"),
	LAND_8("#d6cca6"),
	LAND_9("#c7b699"),
	LAND_10("#bda893"),
	LAND_11("#d2c4b5"),
	LAND_12("#e7dfd7"),

	WATER_1("#cae8f0"),
	WATER_2("#c7e5ee"),
	WATER_3("#c1e1ea"),
	WATER_4("#bddee7"),
	WATER_5("#b7dae3"),
	WATER_6("#b4d7e1"),
	WATER_7("#b0d4de"),
	WATER_8("#abd0da"),
	WATER_9("#a7cdd7"),
	WATER_10("#a3cad5"),
	WATER_11("#9dc6d1"),
	WATER_12("#99c3ce"),
	WATER_13("#94bfca"),
	WATER_14("#90bcc8"),
	WATER_15("#8cb9c5"),
	WATER_16("#86b5c1"),
	WATER_17("#83b2bf"),
	WATER_18("#80b0bd"),

	ICE_1("#ffffff"),
	ICE_2("#fefefe"),
	ICE_3("#f7f7f7"),
	ICE_4("#f1f1f1"),
	ICE_5("#eaeaea"),
	ICE_6("#e3e3e3"),
	ICE_7("#dddddd"),
	ICE_8("#d6d6d6");
	
	private String color;

	private ColorSymbol(String color) {
		this.color = color;
	}
	
	public FeatureType getFeatureType() {
		return FeatureType.valueOf(StringUtils.substringBefore(name(), "_").toUpperCase());
	}

	public int elevation() {
		switch (getFeatureType()) {
		case LAND:
		case ICE:
			return Integer.parseInt(StringUtils.substringAfter(name(), "_"));
		case WATER:
		default:
			return 1 - Integer.parseInt(StringUtils.substringAfter(name(), "_"));
		}
	}

	public String getColor() {
		return color;
	}
	
	public static ColorSymbol fromColor(String color) {
		Optional<ColorSymbol> optional = Arrays.asList(values()).stream().filter(c -> Objects.equals(c.getColor().toUpperCase(), color.toUpperCase())).findFirst();
		if (optional.isPresent()) {
			return optional.get();
		} else {
			System.out.println("Can't find color " + color);
			return null;
		}
	}
	
	public static List<ColorSymbol> orderedValues() {
		List<ColorSymbol> result = Arrays.asList(values());
		result.sort(new Comparator<ColorSymbol>() {
			@Override
			public int compare(ColorSymbol o1, ColorSymbol o2) {
				if (o1 == o2) {
					return 0;
				} else if (o1.getFeatureType() == o2.getFeatureType()) {
					return o1.elevation() - o2.elevation();
				} else {
					return o1.getFeatureType().ordinal() - o2.getFeatureType().ordinal();
				}
			}
		});
		return result;
	}

	public static List<ColorSymbol> landSymbols() {
		return Arrays.asList(values()).stream().filter(s -> s.getFeatureType() == FeatureType.LAND).collect(Collectors.toList());
	}
}
