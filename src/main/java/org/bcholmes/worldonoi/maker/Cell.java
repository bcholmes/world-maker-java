package org.bcholmes.worldonoi.maker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bcholmes.worldonoi.maker.svg.PathProvider;
import org.bcholmes.worldonoi.maker.svg.Segment;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Edge;
import de.alsclo.voronoi.graph.Point;

public class Cell implements PathProvider {
	
	public class SegmentImpl implements Segment {
		Point start;
		Point end;
		Point neighbour;

		public SegmentImpl(Point start, Point end, Point neighbour) {
			this.start = start;
			this.end = end;
			this.neighbour = neighbour;
		}
		
		public Point getStart() {
			return start;
		}
		public Point getEnd() {
			return end;
		}

		public Point getNeighbour() {
			return neighbour;
		}
		public SegmentImpl reverse() {
			return new SegmentImpl(end, start, neighbour);
		}

		@Override
		public Point getCenter() {
			return Cell.this.center;
		}
	}

	public Cell(Point center) {
		this.center = center;
	}
	final Point center;
	int elevation;
	FeatureType featureType;
	List<SegmentImpl> segments = new ArrayList<>();

	public List<SegmentImpl> path() {
		return stitchTogetherPath(this.segments);
	}

	public static List<SegmentImpl> stitchTogetherPath(List<SegmentImpl> segments) {
		List<SegmentImpl> result = new ArrayList<>();
		
		List<SegmentImpl> subPath = new ArrayList<>();
		List<SegmentImpl> temp = new ArrayList<>(segments);
		
		while (!temp.isEmpty()) {
			int count = temp.size();
			for (SegmentImpl segment : new ArrayList<>(temp)) {
				if (subPath.isEmpty()) {
					temp.remove(segment);
					subPath.add(segment);
					break;
				} else if (subPath.get(0).getStart().equals(segment.getEnd())) {
					temp.remove(segment);
					subPath.add(0, segment);
					break;
				} else if (subPath.get(0).getStart().equals(segment.getStart())) {
					temp.remove(segment);
					subPath.add(0, segment.reverse());
					break;
				} else if (subPath.get(subPath.size() - 1).getEnd().equals(segment.getStart())) {
					temp.remove(segment);
					subPath.add(segment);
					break;
				} else if (subPath.get(subPath.size() - 1).getEnd().equals(segment.getEnd())) {
					temp.remove(segment);
					subPath.add(segment.reverse());
					break;
				}
			}
			if (temp.size() == count) {
				result.addAll(subPath);
				subPath.clear();
			}
		}
		
		result.addAll(subPath);
		return result;
	}

	static List<Cell> getCells(Voronoi voronoi) {
		Map<Point,Cell> cellMap = new HashMap<>();
		List<Edge> edges = voronoi.getGraph().edgeStream().collect(Collectors.toList());
		for (Edge edge : edges) {
			Point a = edge.getSite1();
			Point b = edge.getSite2();
			
			Cell cell = cellMap.get(a);
			if (cell == null) {
				cellMap.put(a, cell = new Cell(a));
			}
			if (edge.getA() != null && edge.getB() != null) { 
				cell.segments.add(cell.new SegmentImpl(edge.getA().getLocation(), edge.getB().getLocation(), b));
			}
	
			cell = cellMap.get(b);
			if (cell == null) {
				cellMap.put(b, cell = new Cell(b));
			}
			if (edge.getA() != null && edge.getB() != null) { 
				cell.segments.add(cell.new SegmentImpl(edge.getA().getLocation(), edge.getB().getLocation(), a));
			}
		}
		
		return new ArrayList<>(cellMap.values());
	}

	public Point getCenter() {
		return center;
	}

	public List<SegmentImpl> getSegments() {
		return segments;
	}

	public int getElevation() {
		return elevation;
	}

	public void setElevation(int elevation) {
		this.elevation = elevation;
	}

	public FeatureType getFeatureType() {
		return featureType;
	}

	public void setFeatureType(FeatureType featureType) {
		this.featureType = featureType;
	}
}