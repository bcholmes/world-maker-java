package org.bcholmes.worldonoi.maker;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.bcholmes.worldonoi.maker.file.PointExportFormat;
import org.bcholmes.worldonoi.maker.file.WorldMapExportFormat;
import org.bcholmes.worldonoi.maker.svg.ColorUtil;
import org.bcholmes.worldonoi.maker.svg.Svg;
import org.bcholmes.worldonoi.maker.svg.SvgWriter;
import org.simpleframework.xml.core.Persister;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Point;

public class WorldMaker {
	
	private static final int NUMBER_OF_INITIAL_POINTS = 100000;
	private static final int HEIGHT = 1200;
	private static final int WIDTH = 2400;
	static Random random = new Random();
	static BufferedImage image;
	
	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			System.out.println("Read file " + args[0]);
			WorldMapExportFormat json = read(args[0]);
			System.out.println("Regenerate world map");
			WorldMap worldMap = convert(json);

			System.out.println("Render as SVG");
			renderAsSvg(worldMap, "./target/out.svg");
		} else {
			Size size = new Size(WIDTH, HEIGHT);
			Voronoi voronoi = randomlyGenerateInitialVoronoi(size);
					
			System.out.println("Choose our colours");
			WorldMap worldMap = WorldMapFactory.create(voronoi, size);
			worldMap.getCells().forEach(c -> setCellAttributes(c));
			System.out.println("Write out file");
			writeWorldMapFile(worldMap, "./target/worldMap.json");
			
			System.out.println("Render as SVG");
			renderAsSvg(worldMap, "./target/out.svg");
		}
	}
	
	private static WorldMap convert(WorldMapExportFormat json) {
		Size size = new Size(json.getWidth(), json.getHeight());
		Map<Point, PointExportFormat> map = new HashMap<>();
		for (PointExportFormat p : json.getPoints()) {
			Point point = new Point(p.getCoordinates().get(0), p.getCoordinates().get(1));
			map.put(point, p);
		}

		System.out.println("Adjust edges and regenerate the voronoi");
		Voronoi voronoi = adjustEdges(map.keySet(), size);
		
		System.out.println("Convert to map");
		WorldMap result = WorldMapFactory.create(voronoi, size);
		result.getCells().forEach(c -> { assignAttributesFromFile(map, c, size); });
		return result;
	}

	private static void assignAttributesFromFile(Map<Point, PointExportFormat> map, Cell c, Size size) {
		Point centre = c.center;
		if (centre.x < 0 || centre.x > size.width) {
			centre = new Point((centre.x + size.width) % (double) size.width, centre.y); 
		}
		PointExportFormat p = map.get(centre);
		if (p == null) {
			Optional<Point> nearest = map.keySet().stream().sorted(new Comparator<Point>() {
				@Override
				public int compare(Point o1, Point o2) {
					return new Double(distance(c.center, o1)).compareTo(new Double(distance(c.center, o2)));
				}
			}).findFirst();
			if (nearest.isPresent()) {
				p = map.get(nearest.get());
			}
		}
		if (p == null) {
			throw new RuntimeException("Can't find c: " + centre);
		}
		c.setElevation(p.getElevation()); 
		c.setFeatureType(p.getType());
	}
	
	private static double distance(Point p1, Point p2) {
		double deltaX = p1.x - p2.x;
		double deltaY = p1.y - p2.y;
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}

	private static WorldMapExportFormat read(String fileName) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(new File(fileName), WorldMapExportFormat.class);
	}

	private static Voronoi randomlyGenerateInitialVoronoi(Size size) {
		System.out.println("Generate Random points");
		List<Point> points = generateRandomPoints(size);
		System.out.println("Create initial Voronoi");
		Voronoi voronoi = new Voronoi(points);
		System.out.println("Apply Lloyd's relaxation");		
		voronoi = voronoi.relax();
		System.out.println("Fix our edges");
		voronoi = adjustEdges(voronoi, size);
		return voronoi;
	}

	private static void writeWorldMapFile(WorldMap worldMap, String string) throws JsonGenerationException, JsonMappingException, IOException {
		WorldMapExportFormat export = new WorldMapExportFormat();
		export.setHeight(worldMap.getSize().getHeight());
		export.setWidth(worldMap.getSize().getWidth());
		
		export.setPoints(worldMap.getCells().stream().map(c -> createExportPoint(c)).collect(Collectors.toList()));
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
			objectMapper.writeValue(new File(string), export);
		} finally {
			
		}
	}

	private static PointExportFormat createExportPoint(Cell c) {
		PointExportFormat result = new PointExportFormat();
		result.setCoordinates(Arrays.asList(c.center.x, c.center.y));
		result.setType(c.getFeatureType());
		result.setElevation(c.getElevation());
		return result;
	}

	private static void setCellAttributes(Cell c) {
		ColorSymbol color = chooseColorFromOriginalImage(c);
		c.setElevation(color.elevation());
		c.setFeatureType(color.getFeatureType());
	}

	private static Voronoi adjustEdges(Voronoi voronoi, Size size) {
		return adjustEdges(voronoi.getGraph().getSitePoints(), size);
	}

	private static Voronoi adjustEdges(Set<Point> sitePoints, Size size) {
		Set<Point> revisedPoints = sitePoints.stream().filter(p -> p.x >= 0 && p.x < size.width && p.y >= 0 && p.y < size.height).collect(Collectors.toSet());
		Set<Point> overhangXPoints = sitePoints.stream()
				.filter(p -> p.x < 100 || p.x > (size.width - 100))
				.map(p -> p.x < 100 ? new Point(p.x + size.width, p.y) : new Point(p.x - size.width, p.y))
				.collect(Collectors.toSet());
		revisedPoints.addAll(overhangXPoints);
		Set<Point> overhangYPoints = sitePoints.stream()
				.filter(p -> p.y < 100 || p.y > (size.height - 100))
				.map(p -> p.y < 100 ? new Point(p.x, p.y + size.height) : new Point(p.x, p.y - size.height))
				.collect(Collectors.toSet());
		revisedPoints.addAll(overhangYPoints);
		
		return new Voronoi(revisedPoints);
	}

	private static ColorSymbol chooseColorFromOriginalImage(Cell cell) {
		Point center = cell.center;
		int x = Math.max(0, Math.min((int) Math.round(center.x / 3), getImage().getWidth()-1));
		int y = Math.max(0, Math.min((int) Math.round(center.y / 3), getImage().getHeight()-1));
		
		int rgb = getImage().getRGB(x, y);
		
		Color color = new Color(rgb);
		String colorString = ColorUtil.toHex(color);
		ColorSymbol symbol = ColorSymbol.fromColor(colorString);
		return symbol;
	}

	private static BufferedImage getImage() {
		try {
			if (image == null) {
				image = ImageIO.read(new File("./etc/mariah iv.png"));
			}
			return image;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void renderAsSvg(WorldMap worldMap, String outputLocation)
			throws Exception {
		Svg svg = SvgWriter.toLayeredSvg(worldMap);
		
		FileOutputStream out = new FileOutputStream(new File(outputLocation));
		try {
			out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>".getBytes());
			Persister persister = new Persister();
			persister.write(svg, out);
		} finally {
			out.close();
		}
	}

	private static List<Point> generateRandomPoints(Size size) {
		List<Point> result = new ArrayList<>();
		for (int i = 0; i < NUMBER_OF_INITIAL_POINTS; i++) {
			float x = random.nextFloat() * size.width;
			float y = random.nextFloat() * size.height;
			result.add(new Point(x, y));
		}
		
		return result;
	}
}
