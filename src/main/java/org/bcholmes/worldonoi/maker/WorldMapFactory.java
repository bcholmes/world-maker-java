package org.bcholmes.worldonoi.maker;

import java.util.List;
import java.util.stream.Collectors;

import org.bcholmes.worldonoi.maker.Cell.SegmentImpl;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Point;

public class WorldMapFactory {

	public static WorldMap create(Voronoi voronoi, Size size) {
		List<Cell> cells = Cell.getCells(voronoi).stream().filter(c -> isAtLeastPartiallyContained(c, size)).collect(Collectors.toList());
		
		return new WorldMap(cells, size);
	}

	private static boolean isAtLeastPartiallyContained(Cell cell, Size size) {
		boolean result = false;
		for (SegmentImpl segment : cell.getSegments()) {
			if (isEntirelyContained(segment.getStart(), size) || isEntirelyContained(segment.getEnd(), size)) {
				result = true;
				break;
			}
		}
		return result;
	}

	private static boolean isEntirelyContained(Point point, Size size) {
		return point.x >=0 && point.x <= size.width && point.y >= 0 && point.y <= size.height;
	}

}
