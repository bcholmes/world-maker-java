package org.bcholmes.worldonoi.maker.file;

import java.util.List;

public class WorldMapExportFormat {

	private int width;
	private int height;
	
	List<PointExportFormat> points;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public List<PointExportFormat> getPoints() {
		return points;
	}

	public void setPoints(List<PointExportFormat> points) {
		this.points = points;
	}
}
