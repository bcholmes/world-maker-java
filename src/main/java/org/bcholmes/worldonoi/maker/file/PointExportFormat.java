package org.bcholmes.worldonoi.maker.file;

import java.util.List;

import org.bcholmes.worldonoi.maker.FeatureType;

public class PointExportFormat {

	List<Double> coordinates;
	int elevation;
	FeatureType type;
	
	public List<Double> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(List<Double> coordinates) {
		this.coordinates = coordinates;
	}
	public int getElevation() {
		return elevation;
	}
	public void setElevation(int elevation) {
		this.elevation = elevation;
	}
	public FeatureType getType() {
		return type;
	}
	public void setType(FeatureType type) {
		this.type = type;
	}
}
