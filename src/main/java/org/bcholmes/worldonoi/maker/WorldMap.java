package org.bcholmes.worldonoi.maker;

import java.util.List;

public class WorldMap {

	private List<Cell> cells;
	private Size size;

	public WorldMap(List<Cell> cells, Size size) {
		this.cells = cells;
		this.size = size;
	}

	public List<Cell> getCells() {
		return cells;
	}

	public Size getSize() {
		return size;
	}

}
