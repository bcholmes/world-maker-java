package org.bcholmes.worldonoi.maker;

public enum FeatureType {
	WATER, LAND, ICE
}
