# World Maker

This codebase exists to experiment with and prototype some RPG world mapping algorithms. 

Some of the ideas that I'm playing with, here, relate to concepts documented by [Red Blob Games](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/) using randomly-generated Voronoi diagrams and treatment of elevation.

The part of the exercise that I'm most interested in is coercing the map into different representations depending on different needs: some times you want a quasi-geographical elevation map; some times you want a simplified representation; and some times you just need borders and cities. But to get to the place where I could work with those concepts, I first needed to be able to create a programmatic representation of the map.

I started with a map that I randomly generated from [Donjon's Fractal Map generator](https://donjon.bin.sh/world/). 

![Fractal-generated world map](./etc/mariah%20iv.png "Fractal-generated world map")

Then I randomly generated a Voronoi, and assigned elevation and land features based on the fractal map. Next, I figured out how to create a layered representation of the map, and rendered that as SVG. At any large number of Voronoi centres (say, 100,000), this part of the exercise is slow slow slow, but I'll figure out optimizations later. At lower numbers of points, the end result has a pretty tessellated look.

Here's an image generated from a basic SVG output.

![Voronoi representation of same world map](./etc/mariah%20iv%20voronoi%20small.png "Voronoi representation of same world map")

## File output

Once the final voronoi is created and assigned with attributes, I can just save a file containing the centre points (with those attributes), and then I can re-read that file and I should get the same voronoi. 

## Next Steps

1. I want to support multiple output formats:
    1. _Traveller_-style [20-sided solid map](https://bcholmes.org/rpgs/starhero/triangle.svg)
    2. Rounded _Fading Suns_-style [joined cicle map](https://bcholmes.org/fading_suns/world_vera_cruz.svg)
    3. Something that'd look appropriate in a _Star Trek_ LCARS display.
2. I want to support biomes, much like Red Blob Games describes
3. I want to support multiple notions of "edge roughness" -- sometimes you want a bit more low-res 
4. I want to support jurisdictional borders and cities
5. I want to be able to call out and render individual land masses

Eventually, I think I'll port all of this code to Objective-C and create a GUI editor.